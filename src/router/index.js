import { createRouter, createWebHistory } from 'vue-router'
import ContactView from '../views/Contacts/Index.vue'
import ContactAddEdit from '../views/Contacts/AddEdit.vue'
import ContactDetails from '../views/Contacts/Details.vue'
import Login from '../views/Login.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/contacts',
      name: 'contacts',
      component: ContactView,
      meta:{auth:false},
    },
    {
      path: '/contact/add-edit/',
      name: 'contact-add',
      component: ContactAddEdit,
      meta:{auth:true}
    },
    {
      path: '/contact/add-edit/:id' ,
      name: 'contact-edit',
      component: ContactAddEdit,
      meta:{auth:true}
    },
    {
      path: '/' ,
      name: 'login',
      component: Login,
      meta:{auth:false}
    },
    {
      path: '/contact/details/:id' ,
      name: 'contact-details',
      component: ContactDetails,
      meta:{auth:true}
    }
  ]
})
router.beforeEach((to, from, next) => {
  if (to.meta.auth) {
      let auth = localStorage.getItem('auth');
      if(auth === "true")
        return next();
      else
        router.push({ name: "login" });
  }
  else
    return next();
});
export default router
